﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fixrobot : MonoBehaviour
{
    [SerializeField] bool ArmFixed = false;
    [SerializeField] bool EarFixed = false;
    [SerializeField] bool TailFixed = false;
    public static IsHoldingScrip isholding;

    private void OnTriggerEnter(Collider other)
    {
        if (IsHoldingScrip.HeldObjectID == 6)
        {
            if (ArmFixed == false)
            {
                ArmFixed = true;
            }
            else if (EarFixed == false)
            {
                ArmFixed = true;
            }
            else if (TailFixed == false)
            {
                Application.Quit();
            }
        }
    }
}
