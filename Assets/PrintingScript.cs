﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PrintingScript : MonoBehaviour
{
    public IsHoldingScrip isHoldingScript;

    [SerializeField] GameObject firstCanvas;
    bool FirstRecipie = false;
    [SerializeField] GameObject secondCanvas;
    bool SecondRecipie = false;
    [SerializeField] GameObject ThirdCanvas;
    bool ThirdRecipie = false;

    public int[] allObjectID = new int[2];
    [SerializeField] public static GameObject HeldObject;
    [SerializeField] int addedID;
    [SerializeField] int i;
    bool IsMaking = false;
    [SerializeField] float TimeLeft = 30;


    [SerializeField] GameObject Arm;
    [SerializeField] int[] armID = new int[2];
    bool IsMakingArm = false;

    [SerializeField] GameObject Ears;
    [SerializeField] int[] earsID = new int[2];
    bool IsMakingEars = false;

    bool FirstObject;
    bool SecondObject;
    bool ThirdObject;


    [SerializeField] GameObject Tail;
    [SerializeField] int[] tailID = new int[2];
    bool IsMakingTail = false;
    bool completedProductPickup = false;
    [SerializeField] GameObject madeItem;

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "maincharacter")
        {
            addedID = 0;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (completedProductPickup == true && other.tag == "maincharacter" )
        {
            Debug.Log("here is your birthday present!");
            isHoldingScript.pickup(madeItem, 6);
            completedProductPickup = false;
        }



        if (IsHoldingScrip.testi != null && i < 3 && completedProductPickup == false && other.tag == "maincharacter" || IsHoldingScrip.HeldObjectID==6 && other.tag == "maincharacter")
        {
            // making the first recipie
            if (FirstRecipie == false)
            {
                if (FirstObject == false && addedID != 6)
                {
                    addedID = IsHoldingScrip.HeldObjectID;
                    if (addedID == 1)
                    {
                        Add();
                        IsHoldingScrip.HeldObjectID = 0;
                        addedID = 0;
                        FirstObject = true;
                    }
                    else
                    {
                        errorGameOver();
                    }
                }
                else if (SecondObject == false && addedID != 6)
                {
                    addedID = IsHoldingScrip.HeldObjectID;
                    if (addedID == 4)
                    {
                        Add();
                        IsHoldingScrip.HeldObjectID = 0;
                        addedID = 0;
                        SecondObject = true;
                    }
                    else if (addedID == 0)
                    {
                        Debug.Log("Don't come empty handed!");
                    }
                    else
                    {
                        errorGameOver();
                        IsHoldingScrip.HeldObjectID = 0;
                        addedID = 0;
                    }
                }
                else if (ThirdObject == false && addedID != 6)
                {
                    addedID = IsHoldingScrip.HeldObjectID;
                    if (addedID == 2)
                    {
                        Add();
                        IsHoldingScrip.HeldObjectID = 0;
                        addedID = 0;
                        ThirdObject = true;
                        FirstRecipie = true;
                        firstCanvas.SetActive(false);
                        secondCanvas.SetActive(true);
                    }
                    else if (addedID == 0)
                    {
                        Debug.Log("Don't come empty handed!");
                    }
                    else
                    {
                        errorGameOver();
                        IsHoldingScrip.HeldObjectID = 0;
                        addedID = 0;
                    }
                }

            }
            //making the second recipie
            else if (SecondRecipie == false)
            {
                if (FirstObject == false && addedID != 6)
                {
                    addedID = IsHoldingScrip.HeldObjectID;
                    if (addedID == 4)
                    {
                        Add();
                        IsHoldingScrip.HeldObjectID = 0;
                        addedID = 0;
                        FirstObject = true;
                    }
                    else if (addedID == 0)
                    {
                        Debug.Log("Don't come empty handed!");
                    }
                    else
                    {
                        errorGameOver();
                        IsHoldingScrip.HeldObjectID = 0;
                        addedID = 0;
                    }
                }
                else if (SecondObject == false && addedID != 6)
                {
                    addedID = IsHoldingScrip.HeldObjectID;
                    if (addedID == 3)
                    {
                        Add();
                        IsHoldingScrip.HeldObjectID = 0;
                        addedID = 0;
                        SecondObject = true;
                    }
                    else if (addedID == 0)
                    {
                        Debug.Log("Don't come empty handed!");
                    }
                    else
                    {
                        errorGameOver();
                        IsHoldingScrip.HeldObjectID = 0;
                        addedID = 0;
                    }
                }
                else if (ThirdObject == false && addedID != 6)
                {
                    addedID = IsHoldingScrip.HeldObjectID;
                    if (addedID == 1)
                    {
                        Add();
                        IsHoldingScrip.HeldObjectID = 0;
                        addedID = 0;
                        ThirdObject = true;
                        SecondRecipie = true;
                        secondCanvas.SetActive(false);
                        ThirdCanvas.SetActive(true);
                    }
                    else if (addedID == 0)
                    {
                        Debug.Log("Don't come empty handed!");
                    }
                    else
                    {
                        errorGameOver();
                        IsHoldingScrip.HeldObjectID = 0;
                        addedID = 0;
                    }
                }
            }

            // third recipie
            else if (ThirdRecipie == false)
            {
                if (FirstObject == false)
                {
                    addedID = IsHoldingScrip.HeldObjectID;
                    if (addedID == 2 && addedID != 6)
                    {
                        Add();
                        IsHoldingScrip.HeldObjectID = 0;
                        addedID = 0;
                        FirstObject = true;
                    }
                    else if (addedID == 0)
                    {
                        Debug.Log("Don't come empty handed!");
                    }
                    else
                    {
                        errorGameOver();
                        IsHoldingScrip.HeldObjectID = 0;
                        addedID = 0;
                    }
                }
                else if (SecondObject == false && addedID != 6)
                {
                    addedID = IsHoldingScrip.HeldObjectID;
                    if (addedID == 3)
                    {
                        Add();
                        IsHoldingScrip.HeldObjectID = 0;
                        addedID = 0;
                        SecondObject = true;
                    }
                    else if (addedID == 0)
                    {
                        Debug.Log("Don't come empty handed!");
                    }
                    else
                    {
                        errorGameOver();
                        IsHoldingScrip.HeldObjectID = 0;
                        addedID = 0;
                    }
                }
                else if (ThirdObject == false && addedID != 6)
                {
                    addedID = IsHoldingScrip.HeldObjectID;
                    if (addedID == 2)
                    {
                        Add();
                        IsHoldingScrip.HeldObjectID = 0;
                        addedID = 0;
                        ThirdObject = true;
                        ThirdRecipie = true;
                        ThirdCanvas.SetActive(false);


                    }
                    else if (addedID == 0)
                    {
                        Debug.Log("Don't come empty handed!");
                    }
                    else
                    {
                        errorGameOver();
                        IsHoldingScrip.HeldObjectID = 0;
                        addedID = 0;
                    }
                }
            }
            else if (FirstObject==true && SecondObject==true && ThirdObject == true)
            {
                Debug.Log("ALREADY FULL!");
                isHoldingScript.letgo(HeldObject);
                HeldObject = null;
                
            }
        }

        else if (IsMaking == true)
        {
            Debug.Log("IM BUSY!");
            isHoldingScript.letgo(HeldObject);
            HeldObject = null;
        }  
    }

    public void errorGameOver()
    {
        Debug.Log("WRONG RECIPIE SHUTTING DOWN!");
        isHoldingScript.letgo(HeldObject);
        Application.Quit();
    }

    public void Add()
    {
        isHoldingScript.letgo(HeldObject);
        allObjectID[i] = addedID;
        Debug.Log("added: " + addedID);
        i++;
        
    }

    public void emptylist()
    {
        for (int empty = 0; empty < allObjectID.Length; empty++)
        {
            allObjectID[empty] = 0;
        }
        IsMakingArm = false;
        IsMakingEars = false;
        IsMakingTail = false;
        IsMaking = false;
        TimeLeft = 30f;
        i = 0;
        FirstObject = false;
        SecondObject = false;
        ThirdObject = false;
        IsHoldingScrip.HeldObjectID = 6;
    }

    public void TimerAndWhatIsMade(GameObject item)
    {
        if (IsMaking == true)
        {
            TimeLeft -= Time.deltaTime;
            if (TimeLeft < 0)
            {
                
                Debug.Log("MADE " + item);
                madeItem = item;
                emptylist();
                completedProductPickup = true;


            }
        }
    }
    private void Update()
    {
        if (Input.GetButton("Jump") && i==3 && IsMaking==false)
        {
            
            if (allObjectID.SequenceEqual(armID))
            {
                Debug.Log("making an arm...");
                IsMakingArm = true;
                IsMaking = true;
            }
            if (allObjectID.SequenceEqual(earsID))
            {
                Debug.Log("making a pair of ears...");
                IsMakingEars = true;
                IsMaking = true;
            }
            if (allObjectID.SequenceEqual(tailID))
            {
                Debug.Log("making a tail..");
                IsMakingTail = true;
                IsMaking = true;
            }

        }
        if (IsMakingArm == true)
        {
            TimerAndWhatIsMade(Arm);
        }
        if (IsMakingEars == true)
        {
            TimerAndWhatIsMade(Ears);
        }
        if (IsMakingTail == true)
        {
            TimerAndWhatIsMade(Tail);
        }
    }
}
