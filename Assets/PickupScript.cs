﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupScript : MonoBehaviour
{
    public IsHoldingScrip isHoldingScript;
    [SerializeField] public GameObject MeshObject;
    [SerializeField] public int ObjectID;
    bool CanPickUp;
    bool isholding = false;
    bool onlyonce = false;
 
       private void OnTriggerEnter (Collider collision)
    {
        CanPickUp = true;
        
    }
    private void OnTriggerExit (Collider collision)
    {
        CanPickUp = false;
        onlyonce = false;
    }




    // Update is called once per frame
    void Update()
    {
        if (onlyonce == false)
        {
            if (CanPickUp && isholding == false)
            {
                if (Input.GetButton("Jump") && IsHoldingScrip.HeldObjectID != 6)
                {
                    isHoldingScript.pickup(MeshObject, ObjectID);
                    isholding = true;
                    onlyonce = true;
                }
            }
            else
            {
                if (Input.GetButton("Jump") && IsHoldingScrip.HeldObjectID != 6)
                {
                    isHoldingScript.letgo(MeshObject);
                    isholding = false;
                    onlyonce = true;
                }
            }
        }
        
        
    }
}
