﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsHoldingScrip : MonoBehaviour
{
    [SerializeField] public GameObject ThisObject;
    [SerializeField] public static GameObject testi;
    [SerializeField] public static int HeldObjectID;
    [SerializeField] public static PrintingScript printingScript;
    

    // Start is called before the first frame update
    public void pickup(GameObject importedMesh, int importedID)
    {
        importedMesh.transform.localPosition = new Vector3(0,5,0);
        testi = importedMesh;
        HeldObjectID = importedID;
        PrintingScript.HeldObject = importedMesh;
        
    }

    // Update is called once per frame
    public void letgo (GameObject importedGameobject)
    {
        importedGameobject.transform.localPosition = new Vector3(0, -20, 0);
        HeldObjectID = 0;
    }
}
