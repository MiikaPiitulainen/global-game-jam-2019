﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float speed = 0;
    public Vector3 direction;
    private string level = "broken";
    public bool robotOn = false;
    private bool turnOn = false;


    void Start()
    {
        direction = Quaternion.AngleAxis(Random.Range(-70.0f, 70.0f), Vector3.up) * direction;
        transform.rotation = Quaternion.LookRotation(direction);
    }

    void Update()
    {
        var exit = GameObject.FindWithTag("Exit");
        if (Input.GetKeyDown(KeyCode.R))
        { 
            if(turnOn == false)
            {
                robotOn = true;
                
                turnOn = true;
            }
            
        }


        GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        if (level == "broken" && robotOn == true)
        {
            speed = 20;
            Vector3 newPos = transform.position + direction * speed * Time.deltaTime;
            GetComponent<Rigidbody>().MovePosition(newPos);
     
        }
        if (level == "semi" && robotOn == true)
        {
            speed = 30;
            Vector3 newPos = transform.position + direction * speed * Time.deltaTime;
            GetComponent<Rigidbody>().MovePosition(newPos);
        }
        if (level == "almost" && robotOn == true)
        {
            StopCoroutine("Turn");
            speed = 50;
            Vector3 newPos = transform.position + direction * speed * Time.deltaTime;
            GetComponent<Rigidbody>().MovePosition(newPos);
        }
        if (level == "fixed" && robotOn == true)
        {
            speed = 5;
            transform.LookAt(exit.transform.position);
            transform.position = Vector3.MoveTowards(transform.position, exit.transform.position, (speed*Time.deltaTime));
        }

    }

    void OnCollisionEnter(Collision col)
    {
        Debug.Log("Collision");
        if (col.gameObject.tag == "Wall")
        {
            direction = col.contacts[0].normal;
            direction = Quaternion.AngleAxis(Random.Range(-70.0f, 70.0f), Vector3.up) * direction;
            transform.rotation = Quaternion.LookRotation(direction);
        }
        if (col.gameObject.tag == "Crate")
        {
            direction = col.contacts[0].normal;
            direction = Quaternion.AngleAxis(Random.Range(-70.0f, 70.0f), Vector3.up) * direction;
            transform.rotation = Quaternion.LookRotation(direction);
            
            int childCount = col.transform.childCount;
            
            for (int i = 0; i < childCount; i++)
            {
                Destroy(col.transform.GetChild(i).gameObject);
            }
            Destroy(col.gameObject);
        }
        if (col.gameObject.tag == "Component")
        {
            if(level == "broken")
            {
                Destroy(col.gameObject);
                level = "semi";
            }
            if (level == "semi")
            {
                Destroy(col.gameObject);
                level = "almost";
            }
            if (level == "almost")
            {
                Destroy(col.gameObject);
                
                level = "fixed";
            }



        }

    }
}